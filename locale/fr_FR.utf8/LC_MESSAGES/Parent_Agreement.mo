��          <      \       p      q      x   O   �     �      �     �  d   �                   Accept Parent Agreement Your parents must login first and accept the Parent Agreement so you can login. Project-Id-Version: Parent Agreement plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-31 17:44+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: dgettext:2
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.2.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Accepter Accord Parents Vos parents doivent d'abord se connecter et accepter l'Accord Parents pour que vous puissiez entrer. 